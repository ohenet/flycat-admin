import request from '@/utils/request'
/**
 * 登录
 * @param data
 * @returns {AxiosPromise}
 */
export function login (data) {
  return request({
    url: '/login',
    method: 'post',
    data: data
  })
}

/**
 * 登出
 * @returns {AxiosPromise}
 */
export function logout () {
  return request({
    url: '/logout',
    method: 'post'
  })
}
