import request from '@/utils/request'

/**
 * 获取管理员信息
 * @returns {AxiosPromise}
 */
export function getInfo () {
  return request({
    url: '/auth/info',
    method: 'get'
  })
}

/**
 * 获取后台授权菜单
 * @returns {AxiosPromise}
 */
export function getAuthMenus () {
  return request({
    url: '/auth/menus',
    method: 'get'
  })
}
