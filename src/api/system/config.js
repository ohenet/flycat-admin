import request from '@/utils/request'
/**
 * 获取管理员信息
 * @returns {AxiosPromise}
 */
export function getList (data) {
  return request({
    url: '/system/config/list',
    method: 'get',
    params: data
  })
}