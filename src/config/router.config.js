// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'
import { home,setting } from '@/core/icons'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/index',
    children: [
      {
        path: '/index',
        name: 'index',
        component: () => import('@/views/index'),
        meta: { title: '首页', icon: home, keepAlive: false, permission: ['/index'] }
      },
      // dashboard
      {
        path: '/system',
        name: '系统',
        redirect: '/system/setting',
        component: RouteView,
        meta: { title: '首页', keepAlive: true, icon: setting, permission: ['/system'] },
        children: [
          {
            path: '/system/setting',
            name: 'system_setting',
            component: () => import('@/views/system/setting'),
            meta: { title: '系统设置', keepAlive: true, permission: ['/system/setting'] }
          },
          {
            path: '/system/system_config',
            name: 'system_config',
            component: () => import('@/views/system/system_config'),
            meta: { title: '配置管理', keepAlive: true, permission: ['/system/system_config'] }
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/auth',
    component: UserLayout,
    redirect: '/auth/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/auth/Login')
      }
    ]
  },
  {
    path: '/system',
    component: BasicLayout,
    redirect: '/system/setting',
    hidden: true,
    children: [
      {
        path: 'setting',
        name: 'setting',
        component: () => import(/* webpackChunkName: "user" */ '@/views/system/setting')
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
