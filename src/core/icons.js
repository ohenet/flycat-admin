/**
 * Custom icon list
 * All icons are loaded here for easy management
 * @see https://vue.ant.design/components/icon/#Custom-Font-Icon
 *
 * 自定义图标加载表
 * 所有图标均从这里加载，方便管理
 */
import home from '@/assets/icons/home-fill.svg?inline'
import setting from '@/assets/icons/setting-fill.svg?inline'
export {
  home,
  setting
}
