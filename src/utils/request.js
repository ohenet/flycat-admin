import axios from 'axios'
import store from '@/store'
import storage from 'store'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
import { ACCESS_TOKEN, ACCESS_TOKEN_PREFIX } from '@/store/mutation-types'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 6000 // 请求超时时间
})
axios.defaults.withCredentials = true// 携带cookie

// 请求拦截器
request.interceptors.request.use(
  config => {
    const token = storage.get(ACCESS_TOKEN)
    // 如果 token 存在
    // 让每个请求携带自定义 token 请根据实际情况自行修改
    if (token) {
      config.headers[ACCESS_TOKEN] = ACCESS_TOKEN_PREFIX + token
    }
    return config
  },
  error => {
    // do something with request error
    return Promise.reject(error)
  }
)
// 响应拦截
request.interceptors.response.use(
  response => {
    const result = response.data
    // 从 localstorage 获取 token
    const token = storage.get(ACCESS_TOKEN)
    // 状态码0正常，401未登录或登录失效，其他详见文档
    switch (result.code) {
      case 0:
        return response.data
      case 401:
        notification.error({
          message: '错误',
          description: result.msg || '请您登录后操作'
        })
        if (token) {
          store.dispatch('Logout').then(() => {
            setTimeout(() => {
              window.location.reload()
            }, 1000)
          })
        }
        return Promise.reject(result)
      default:
        return Promise.reject(result)
    }
  },
  error => {
    // 网络请求出错
    const errMsg = ((error.response || {}).data || {}).message || '请求出现错误，请稍后再试'
    notification.error({
      message: '网络请求出错',
      description: errMsg,
      duration: 3
    })
    return Promise.reject(error)
  }
)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
